from fastapi import FastAPI
from typing import Optional
from pydantic import BaseModel


app = FastAPI()

#parth operation decorator to the module of a function
@app.get('/blog')
#path operation function
#only get 10 published blogs
def index(limit=10, published : bool = True, sort: Optional[str] = None):

    if published:
        return {'data': f'{limit} published blog from the db'}
    else:
        return {'data': f'{limit} blog from the db'}    

@app.get('/blog/unpublished')
def unpublished():
    return {'data': 'all unpublished blogs'}

# fetch blog with id = id
@app.get('/blog/{id}')
def showt(id:int):
     return  {'data': id}



@app.get('/blog/{id}/comments')
def comments(id, limit=10):
    #fetch comments of blog with id = id
    return{'data': {'1', '2'} }

#This is the Model
class Blog(BaseModel):
    title: str
    body: str
    published: Optional[bool]


@app.post('/blog')
def create_blog(request: Blog):
    
    return {'data': f"Blog is created with title as {request.title}"}


# for debugging purposes 
# __name__ == "__main__":
#   uvicorn.run(app,host="127.0.0.1"
