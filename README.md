# Fastapi
Creating a CRUP with FastApi

## Project under development 

# Requirement: 
Python 3.6+


# How to run

##all files were created within the blog directory
1. Create an environment: 
python -m venv env

2. Activate venv environment: 
env\Scripts\activate 

3. Install packages in the environment: 
pip install fastapi
pip install uvicorn
pip install sqlalchemy


4. Create the main.py file to assemble the app structure: 
from fastapi import FastAPI

app = FastAPI()

5. Create the blog.sql file: 
This file is extremely important to establish the 
route of the database with the app 

6. Create the database.py file to run the scripts: 
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

SQLALCHAMY_DATABASE_URL = 'sqlite:///./blog.sql'

engine = create_engine(SQLALCHAMY_DATABASE_URL, connect_args={
                       "check_same_thread": False})

SessionLocal = sessionmaker(bind=engine, autocommit=False, autoflush=False,)

Base = declarative_base()

These scripts will implement the database with the app. 
Use the database software of your choice (I used TablePlus)

7. In the main.py file, write the script to complete the database implementation with the app: 
from .database import engine, SessionLocal
from sqlalchemy.orm import Session


models.Base.metadata.create_all(engine)


8. Create the schemas.py file to establish, validate and analyze data and write the scripts: 
from pydantic import BaseModel

class Blog(BaseModel):
    title: str
    body: str



9. Create the models.py file to assemble the database structure: 
from sqlalchemy import Column, Integer, String
from .database import Base



class Blog(Base):
    __tablename__ = 'blogs'

    id = Column(Integer,primary_key=True,index=True)
    title = Column(String)
    body = Column(String)
 
##Okay, so we can create our routes on main.py within the blog directory


Start API: 

uvicorn blog.main:app --reload 



# How to test routes 
1. Access the swagger ui: 
http://127.0.0.1:8000/docs
in the swagger we find all our routes where we can perform the tests
