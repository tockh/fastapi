from pydantic import BaseModel



class Blog(BaseModel):
    title: str
    body: str
#his hid the id parameter when we asked for it in GET/ Blog / {id}Show on Swagger UI

class ShowBlog(BaseModel):
    title: str
    class Config():
        orm_mode = True