from fastapi import FastAPI, Depends, status, Response, HTTPException
from . import  schemas, models
from .database import engine, SessionLocal
from sqlalchemy.orm import Session

app = FastAPI()

#this line creates the modals tables in the database
models.Base.metadata.create_all(engine)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

#in status_code, when typing the action performed, the vs.code itself completes with the http parameter for you (ex: when typing status_code = status.created + tab, the interpreter corrects it for HTTP_201_CREATED). Valid for other status_code
#depends causes us to create sessions to say the default value depending on the db, so once we have done that, we will get the db instance
@app.post('/blog', status_code=status.HTTP_201_CREATED)
def create(request: schemas.Blog, db: Session = Depends(get_db)):
    new_blog = models.Blog(title=request.title, body=request.body)
    db.add(new_blog)
    db.commit()
    db.refresh(new_blog)
    return new_blog

@app.delete('/blog/{id}', status_code=status.HTTP_204_NO_CONTENT)
def destroy(id, db: Session = Depends(get_db)):
    db.query(models.Blog).filter(models.Blog.id == id)
    if not blog.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Blog with id {id} not found")
    blog.delete(synchronize_session=False)
    db.commit()
    return 'done'
    
    
@app.put('/blog/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: schemas.Blog, db: Session = Depends(get_db)):
    blog = db.query(models.Blog).filter(models.Blog.id == id)
    if not blog.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Blog with id {id} not found")
    blog.update(request)    
    db.commit()
    return 'updated'
    
@app.get('/blog')
def all(db: Session = Depends(get_db)):
    blogs = db.query(models.Blog).all()
    return blogs


#the response_model=schemas.ShowBlog hid the id parameter when we asked for it in GET Blog /{id}Show on Swagger UI
@app.get('/blog/{id}', status_code=200, response_model=schemas.ShowBlog)
def show(id,response: Response, db: Session = Depends(get_db)):
    blog = db.query(models.Blog).filter(models.Blog.id == id).first()
    if not blog:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Blog with the id {id} is not available')
    #Above, we’ll run another test with HTTPException to return this status_code faster
        #response.status_code = status.HTTP_404_NOT_FOUND
        #return {'detail':f'Blog with the id{id} is not available"}
    return blog
    
    
#improve this comment   
#So that was really really great and how we can respond with a status code when you created a api it's absolutely important to provide a better status code so that the consumer of your api or the developer who is using your api we're going to get the exact idea what this response is